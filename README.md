# Conducted
This is a mod for the Conducted texture pack. Delver allows you to apply linear filtering to sprite and tile textures. But it does not let you apply linear filtering to mesh textures. This mod applies linear filtering to all textures.

Requires [Delver ModLoader](https://gitlab.com/ZephaniahNoah/delver-modloader).
