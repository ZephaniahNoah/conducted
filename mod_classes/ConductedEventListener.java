package mod_classes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.interrupt.dungeoneer.Art;
import com.interrupt.dungeoneer.game.Level;
import com.zephaniahnoah.delver.ModEventListener;

public class ConductedEventListener extends ModEventListener {

	@Override
	public void levelPostInit(Level l) {
		for (Texture t : Art.cachedTextures.values()) {
			t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		}
	}
}